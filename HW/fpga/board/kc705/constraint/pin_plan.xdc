set_property IOSTANDARD LVCMOS25 [get_ports {i_emdint i_e*}]
#set_property IOSTANDARD LVDS     [get_ports {i_gmiiclk_*}]
set_property IOSTANDARD LVCMOS25 [get_ports io_emdio]
set_property IOSTANDARD LVCMOS25 [get_ports {o_etxd[*]}]
set_property IOSTANDARD LVCMOS25 [get_ports o_erefclk]
set_property IOSTANDARD LVCMOS25 [get_ports o_emdc]
set_property IOSTANDARD LVCMOS25 [get_ports o_erstn]
set_property IOSTANDARD LVCMOS25 [get_ports o_etx_en]
set_property IOSTANDARD LVCMOS25 [get_ports o_etx_er]
set_property IOSTANDARD LVCMOS15 [get_ports {o_led[*]}]
set_property IOSTANDARD LVCMOS15 [get_ports {i_dip[*]}]
set_property IOSTANDARD LVTTL [get_ports sd_*]
#set_property IOSTANDARD LVCMOS25 [get_ports sd_reset]
#set_property IOSTANDARD LVCMOS25 [get_ports sd_detect]
# on board differential clock, 200MHz
set_property PACKAGE_PIN AD12 [get_ports clk_p]
set_property PACKAGE_PIN AD11 [get_ports clk_n]
set_property IOSTANDARD DIFF_SSTL15 [get_ports clk_n]

# Reset active high SW4.1 User button South
set_property VCCAUX_IO DONTCARE [get_ports rst_top]
set_property IOSTANDARD LVCMOS15 [get_ports rst_top]
set_property PACKAGE_PIN AB7 [get_ports rst_top]

# UART Pins
set_property PACKAGE_PIN M19 [get_ports rxd]
set_property IOSTANDARD LVCMOS25 [get_ports rxd]
set_property PACKAGE_PIN K24 [get_ports txd]
set_property IOSTANDARD LVCMOS25 [get_ports txd]
set_property PACKAGE_PIN K23 [get_ports rts]
set_property IOSTANDARD LVCMOS25 [get_ports rts]
set_property PACKAGE_PIN L27 [get_ports cts]
set_property IOSTANDARD LVCMOS25 [get_ports cts]

# SD/SPI Pins
#set_property PACKAGE_PIN AC21 [get_ports spi_cs]
#set_property IOSTANDARD LVCMOS25 [get_ports spi_cs]
#set_property PACKAGE_PIN AB23 [get_ports spi_sclk]
#set_property IOSTANDARD LVCMOS25 [get_ports spi_sclk]
#set_property PACKAGE_PIN AB22 [get_ports spi_mosi]
#set_property IOSTANDARD LVCMOS25 [get_ports spi_mosi]
#set_property PACKAGE_PIN AC20 [get_ports spi_miso]
#set_property IOSTANDARD LVCMOS25 [get_ports spi_miso]
# Is this right ?
set_property PACKAGE_PIN Y21 [get_ports sd_reset]
set_property PACKAGE_PIN AA21 [get_ports sd_detect]
set_property PACKAGE_PIN AB22 [get_ports sd_cmd]
set_property PACKAGE_PIN AB23 [get_ports sd_sclk]
set_property PACKAGE_PIN AA22 [get_ports {sd_dat[2]}]
set_property PACKAGE_PIN AA23 [get_ports {sd_dat[1]}]
set_property PACKAGE_PIN AC20 [get_ports {sd_dat[0]}]
set_property PACKAGE_PIN AC21 [get_ports {sd_dat[3]}]
#
#set_property PACKAGE_PIN G8 [get_ports i_gmiiclk_p]
#set_property PACKAGE_PIN G7 [get_ports i_gmiiclk_n]
set_property PACKAGE_PIN J21 [get_ports io_emdio]
set_property PACKAGE_PIN K30 [get_ports o_erefclk]
set_property PACKAGE_PIN L20 [get_ports o_erstn]
set_property PACKAGE_PIN M27 [get_ports o_etx_en]
set_property PACKAGE_PIN M28 [get_ports i_etx_clk]
set_property PACKAGE_PIN N27 [get_ports {o_etxd[0]}]
set_property PACKAGE_PIN N25 [get_ports {o_etxd[1]}]
set_property PACKAGE_PIN M29 [get_ports {o_etxd[2]}]
set_property PACKAGE_PIN L28 [get_ports {o_etxd[3]}]
set_property PACKAGE_PIN J26 [get_ports {o_etxd[4]}]
set_property PACKAGE_PIN K26 [get_ports {o_etxd[5]}]
set_property PACKAGE_PIN L30 [get_ports {o_etxd[6]}]
set_property PACKAGE_PIN J28 [get_ports {o_etxd[7]}]
set_property PACKAGE_PIN N29 [get_ports o_etx_er]
set_property PACKAGE_PIN N30 [get_ports i_emdint]
set_property PACKAGE_PIN R23 [get_ports o_emdc]
set_property PACKAGE_PIN R28 [get_ports i_erx_dv]
set_property PACKAGE_PIN R30 [get_ports i_erx_crs]
set_property PACKAGE_PIN U27 [get_ports i_erx_clk]
set_property PACKAGE_PIN V26 [get_ports i_erx_er]
#set_property PACKAGE_PIN W19 [get_ports i_erx_col]
set_property PACKAGE_PIN U30 [get_ports {i_erxd[0]}]
set_property PACKAGE_PIN U25 [get_ports {i_erxd[1]}]
set_property PACKAGE_PIN T25 [get_ports {i_erxd[2]}]
set_property PACKAGE_PIN U28 [get_ports {i_erxd[3]}]
set_property PACKAGE_PIN R19 [get_ports {i_erxd[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_erxd[4]}]
set_property PACKAGE_PIN T27 [get_ports {i_erxd[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_erxd[5]}]
set_property PACKAGE_PIN T26 [get_ports {i_erxd[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_erxd[6]}]
set_property PACKAGE_PIN T28 [get_ports {i_erxd[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_erxd[7]}]

set_property PACKAGE_PIN Y29 [get_ports {i_dip[0]}]
set_property PACKAGE_PIN W29 [get_ports {i_dip[1]}]
set_property PACKAGE_PIN AA28 [get_ports {i_dip[2]}]
set_property PACKAGE_PIN Y28 [get_ports {i_dip[3]}]

set_property PACKAGE_PIN AB8 [get_ports {o_led[0]}]
set_property PACKAGE_PIN AA8 [get_ports {o_led[1]}]
set_property PACKAGE_PIN AC9 [get_ports {o_led[2]}]
set_property PACKAGE_PIN AB9 [get_ports {o_led[3]}]
set_property PACKAGE_PIN AE26 [get_ports {o_led[4]}]
set_property PACKAGE_PIN G19 [get_ports {o_led[5]}]
set_property PACKAGE_PIN E18 [get_ports {o_led[6]}]
set_property PACKAGE_PIN F16 [get_ports {o_led[7]}]
# These are dummies that just go to unused LEDs
set_property PACKAGE_PIN Y23 [get_ports PS2_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports PS2_CLK]
set_property PACKAGE_PIN Y24 [get_ports PS2_DATA]
set_property IOSTANDARD LVCMOS33 [get_ports PS2_DATA]

# BUTTON
set_property PACKAGE_PIN AA12 [get_ports GPIO_SW_N]
set_property PACKAGE_PIN AB12 [get_ports GPIO_SW_S]
set_property PACKAGE_PIN AC6 [get_ports GPIO_SW_W]
set_property PACKAGE_PIN AG5 [get_ports GPIO_SW_E]
set_property PACKAGE_PIN G12 [get_ports GPIO_SW_C]

set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_N]
set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_S]
set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_W]
set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_E]
set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_C]

# Set DCI_CASCADE for DDR3 interface
# This needs review
set_property CFGBVS GND [current_design]
set_property CONFIG_VOLTAGE 1.5 [current_design]
# Hack to avoid using GTX
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets i_gmiiclk_p_IBUF]

#set_property PACKAGE_PIN U27 [get_ports PHY_RXCLK]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_RXCLK]
#set_property PACKAGE_PIN V26 [get_ports PHY_RXER]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_RXER]
#set_property PACKAGE_PIN R28 [get_ports PHY_RXCTL_RXDV]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_RXCTL_RXDV]
#set_property PACKAGE_PIN M28 [get_ports PHY_TXCLK]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_TXCLK]
#set_property PACKAGE_PIN N29 [get_ports PHY_TXER]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_TXER]
#set_property PACKAGE_PIN K30 [get_ports PHY_TXC_GTXCLK]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_TXC_GTXCLK]
#set_property PACKAGE_PIN M27 [get_ports PHY_TXCTL_TXEN]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_TXCTL_TXEN]
#set_property PACKAGE_PIN W19 [get_ports PHY_COL]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_COL]
#set_property PACKAGE_PIN R30 [get_ports PHY_CRS]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_CRS]
#set_property PACKAGE_PIN N30 [get_ports PHY_INT]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_INT]
#set_property PACKAGE_PIN R23 [get_ports PHY_MDC]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_MDC]
#set_property PACKAGE_PIN J21 [get_ports PHY_MDIO]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_MDIO]
#set_property PACKAGE_PIN L20 [get_ports PHY_RESET]
#set_property IOSTANDARD LVCMOS25 [get_ports PHY_RESET]
#set_property PACKAGE_PIN H5 [get_ports SGMII_RX_N]
#set_property PACKAGE_PIN H6 [get_ports SGMII_RX_P]
#set_property PACKAGE_PIN J3 [get_ports SGMII_TX_N]
#set_property PACKAGE_PIN J4 [get_ports SGMII_TX_P]
#set_property PACKAGE_PIN G7 [get_ports SGMIICLK_Q0_N]
#set_property PACKAGE_PIN G8 [get_ports SGMIICLK_Q0_P]

















