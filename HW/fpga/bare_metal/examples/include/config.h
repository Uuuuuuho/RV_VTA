/* Automatically generated - do not edit */
#define CONFIG_BOARDDIR board/LowRISC/ariane
#include <config_defaults.h>
#include <config_uncmd_spl.h>
#include <configs/lowrisc-ariane.h>
#include <asm/config.h>
#include <linux/kconfig.h>
#include <config_fallbacks.h>
