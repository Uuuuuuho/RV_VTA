-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2018.1
-- Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity reset_mem is
port (
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    ap_start : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_idle : OUT STD_LOGIC;
    ap_ready : OUT STD_LOGIC;
    sram_idx_V_read : IN STD_LOGIC_VECTOR (15 downto 0);
    range_V : IN STD_LOGIC_VECTOR (15 downto 0);
    mem_V_Addr_A : OUT STD_LOGIC_VECTOR (31 downto 0);
    mem_V_EN_A : OUT STD_LOGIC;
    mem_V_WEN_A : OUT STD_LOGIC_VECTOR (15 downto 0);
    mem_V_Din_A : OUT STD_LOGIC_VECTOR (127 downto 0);
    mem_V_Dout_A : IN STD_LOGIC_VECTOR (127 downto 0);
    ap_return : OUT STD_LOGIC_VECTOR (15 downto 0) );
end;


architecture behav of reset_mem is 
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_ST_fsm_state1 : STD_LOGIC_VECTOR (1 downto 0) := "01";
    constant ap_ST_fsm_state2 : STD_LOGIC_VECTOR (1 downto 0) := "10";
    constant ap_const_lv32_0 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000000";
    constant ap_const_lv32_1 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000001";
    constant ap_const_lv1_0 : STD_LOGIC_VECTOR (0 downto 0) := "0";
    constant ap_const_lv16_0 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
    constant ap_const_lv16_FFFF : STD_LOGIC_VECTOR (15 downto 0) := "1111111111111111";
    constant ap_const_lv128_lc_1 : STD_LOGIC_VECTOR (127 downto 0) := "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    constant ap_const_lv32_4 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000100";
    constant ap_const_lv16_1 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000001";
    constant ap_const_lv1_1 : STD_LOGIC_VECTOR (0 downto 0) := "1";
    constant ap_const_boolean_1 : BOOLEAN := true;

    signal ap_CS_fsm : STD_LOGIC_VECTOR (1 downto 0) := "01";
    attribute fsm_encoding : string;
    attribute fsm_encoding of ap_CS_fsm : signal is "none";
    signal ap_CS_fsm_state1 : STD_LOGIC;
    attribute fsm_encoding of ap_CS_fsm_state1 : signal is "none";
    signal tmp_2_fu_85_p2 : STD_LOGIC_VECTOR (15 downto 0);
    signal tmp_2_reg_123 : STD_LOGIC_VECTOR (15 downto 0);
    signal i_1_fu_96_p2 : STD_LOGIC_VECTOR (15 downto 0);
    signal ap_CS_fsm_state2 : STD_LOGIC;
    attribute fsm_encoding of ap_CS_fsm_state2 : signal is "none";
    signal tmp_1_fu_107_p2 : STD_LOGIC_VECTOR (15 downto 0);
    signal exitcond1_fu_91_p2 : STD_LOGIC_VECTOR (0 downto 0);
    signal t_V_reg_65 : STD_LOGIC_VECTOR (15 downto 0);
    signal i_reg_74 : STD_LOGIC_VECTOR (15 downto 0);
    signal tmp_4_fu_102_p1 : STD_LOGIC_VECTOR (63 downto 0);
    signal mem_V_Addr_A_orig : STD_LOGIC_VECTOR (31 downto 0);
    signal ap_NS_fsm : STD_LOGIC_VECTOR (1 downto 0);


begin




    ap_CS_fsm_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_CS_fsm <= ap_ST_fsm_state1;
            else
                ap_CS_fsm <= ap_NS_fsm;
            end if;
        end if;
    end process;


    i_reg_74_assign_proc : process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((exitcond1_fu_91_p2 = ap_const_lv1_0) and (ap_const_logic_1 = ap_CS_fsm_state2))) then 
                i_reg_74 <= i_1_fu_96_p2;
            elsif (((ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
                i_reg_74 <= ap_const_lv16_0;
            end if; 
        end if;
    end process;

    t_V_reg_65_assign_proc : process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((exitcond1_fu_91_p2 = ap_const_lv1_0) and (ap_const_logic_1 = ap_CS_fsm_state2))) then 
                t_V_reg_65 <= tmp_1_fu_107_p2;
            elsif (((ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
                t_V_reg_65 <= sram_idx_V_read;
            end if; 
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_state1))) then
                tmp_2_reg_123 <= tmp_2_fu_85_p2;
            end if;
        end if;
    end process;

    ap_NS_fsm_assign_proc : process (ap_start, ap_CS_fsm, ap_CS_fsm_state1, ap_CS_fsm_state2, exitcond1_fu_91_p2)
    begin
        case ap_CS_fsm is
            when ap_ST_fsm_state1 => 
                if (((ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_state1))) then
                    ap_NS_fsm <= ap_ST_fsm_state2;
                else
                    ap_NS_fsm <= ap_ST_fsm_state1;
                end if;
            when ap_ST_fsm_state2 => 
                if (((exitcond1_fu_91_p2 = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_state2))) then
                    ap_NS_fsm <= ap_ST_fsm_state1;
                else
                    ap_NS_fsm <= ap_ST_fsm_state2;
                end if;
            when others =>  
                ap_NS_fsm <= "XX";
        end case;
    end process;
    ap_CS_fsm_state1 <= ap_CS_fsm(0);
    ap_CS_fsm_state2 <= ap_CS_fsm(1);

    ap_done_assign_proc : process(ap_start, ap_CS_fsm_state1, ap_CS_fsm_state2, exitcond1_fu_91_p2)
    begin
        if ((((ap_start = ap_const_logic_0) and (ap_const_logic_1 = ap_CS_fsm_state1)) or ((exitcond1_fu_91_p2 = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_state2)))) then 
            ap_done <= ap_const_logic_1;
        else 
            ap_done <= ap_const_logic_0;
        end if; 
    end process;


    ap_idle_assign_proc : process(ap_start, ap_CS_fsm_state1)
    begin
        if (((ap_start = ap_const_logic_0) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            ap_idle <= ap_const_logic_1;
        else 
            ap_idle <= ap_const_logic_0;
        end if; 
    end process;


    ap_ready_assign_proc : process(ap_CS_fsm_state2, exitcond1_fu_91_p2)
    begin
        if (((exitcond1_fu_91_p2 = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_state2))) then 
            ap_ready <= ap_const_logic_1;
        else 
            ap_ready <= ap_const_logic_0;
        end if; 
    end process;

    ap_return <= tmp_2_reg_123;
    exitcond1_fu_91_p2 <= "1" when (i_reg_74 = range_V) else "0";
    i_1_fu_96_p2 <= std_logic_vector(unsigned(i_reg_74) + unsigned(ap_const_lv16_1));
    mem_V_Addr_A <= std_logic_vector(shift_left(unsigned(mem_V_Addr_A_orig),to_integer(unsigned('0' & ap_const_lv32_4(31-1 downto 0)))));
    mem_V_Addr_A_orig <= tmp_4_fu_102_p1(32 - 1 downto 0);
    mem_V_Din_A <= ap_const_lv128_lc_1;

    mem_V_EN_A_assign_proc : process(ap_CS_fsm_state2)
    begin
        if ((ap_const_logic_1 = ap_CS_fsm_state2)) then 
            mem_V_EN_A <= ap_const_logic_1;
        else 
            mem_V_EN_A <= ap_const_logic_0;
        end if; 
    end process;


    mem_V_WEN_A_assign_proc : process(ap_CS_fsm_state2, exitcond1_fu_91_p2)
    begin
        if (((exitcond1_fu_91_p2 = ap_const_lv1_0) and (ap_const_logic_1 = ap_CS_fsm_state2))) then 
            mem_V_WEN_A <= ap_const_lv16_FFFF;
        else 
            mem_V_WEN_A <= ap_const_lv16_0;
        end if; 
    end process;

    tmp_1_fu_107_p2 <= std_logic_vector(unsigned(t_V_reg_65) + unsigned(ap_const_lv16_1));
    tmp_2_fu_85_p2 <= std_logic_vector(unsigned(range_V) + unsigned(sram_idx_V_read));
    tmp_4_fu_102_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(t_V_reg_65),64));
end behav;
